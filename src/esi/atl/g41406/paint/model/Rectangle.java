/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esi.atl.g41406.paint.model;

public class Rectangle implements Shape {

    protected Point origin;
    protected double width;
    protected double height;

    protected char color;

    @Override
    public Rectangle move(double dx, double dy) {
        origin.move(dx, dy);
        return this;
    }

    @Override
    public Rectangle setColor(char color) {
        this.color = color;
        return this;
    }

    public Rectangle move(Point move) {
        origin = move;
        return this;
    }

    @Override
    public Boolean isInside(Point p) {
        return ((p.getX() >= origin.getX())
                && (p.getY() >= origin.getY())
                && (p.getX() <= (origin.getX() + width))
                && (p.getY() <= (origin.getY() + height)));
    }

    @Override
    public char getColor() {
        return color;
    }

    @Override
    public String getCmd() {
        String[] array = {
            "add",
            "rectangle",
            origin.toString2(),
            String.valueOf(width),
            String.valueOf(height),
            String.valueOf(color)};

        return Tools.implode(array, ' ');
    }

    public Rectangle(Point origin, double width, double height) {
        this.origin = new Point(origin.getX(), origin.getY());
        this.width = width;
        this.height = height;
    }

    public Rectangle(Point origin, double width, double height, char color) {
        this(origin, width, height);
        this.color = color;
    }
    public Point getOrigin(){
        return origin;
    }
    public double getWidth(){
        return width;
    }
    public double getHeight(){
        return height;
    }
    @Override
    public String toString() {
        return "Rectangle de centre " + origin + " et ayant comme dimensions :" + width + "x" + height;
    }
}
