/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esi.atl.g41406.paint.model;

public interface Shape {

    Shape move(double dx, double dy);

    Boolean isInside(Point p);

    char getColor();

    Shape setColor(char color);

    String getCmd();
}
