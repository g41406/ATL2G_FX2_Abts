/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esi.atl.g41406.paint.model;

/**
 *
 * @author T5UN4M1
 */
public class Square extends Rectangle {

    public Square(Point origin, double side) {
        super(origin, side, side);
    }

    public Square(Point origin, double side, char color) {
        super(origin, side, side, color);
    }

    @Override
    public String toString() {
        return "Carré ayant comme coordonnée de point haut gauche " + origin + " et ayant comme taille de coté : " + width;
    }

    @Override
    public String getCmd() {
        String[] array = {
            "add",
            "square",
            origin.toString2(),
            String.valueOf(width),
            String.valueOf(color)};

        return Tools.implode(array, ' ');
    }
}
