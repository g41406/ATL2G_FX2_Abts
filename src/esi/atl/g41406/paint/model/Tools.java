/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esi.atl.g41406.paint.model;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import static java.lang.Integer.parseInt;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 *
 * @author T5UN4M1
 */
public class Tools {
    
    static Drawing scene;
    
    static void saveToFile(String path) {
        try {
            PrintWriter writer;
            writer = new PrintWriter(path, "UTF-8");
            writer.print(scene.getCmd());
            writer.close();
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            System.out.println("Couldn't save , error");
        }
    }

    static void loadFromFile(String path) {
        try {
            byte[] encoded = Files.readAllBytes(Paths.get(path));
            String str = new String(encoded, StandardCharsets.UTF_8);
            String[] cmd = str.split("~");
            for (int i = 0; i < cmd.length; ++i) {
                parseCmd(cmd[i]);
            }
        } catch (IOException ex) {
            System.out.println("Couldn't load , error");
        }
    }

    public static String implode(String[] items, char separator) {
        String str = "";
        for (int i = 0; i < items.length; ++i) {
            str += items[i] + ((i != (items.length - 1)) ? separator : "");
        }
        return str;
    }

    public static Point parsePoint(String point) {
        String[] pt = point.split(",");
        return new Point(
                Double.parseDouble(pt[0]),
                Double.parseDouble(pt[1]));
    }

    public static void parseShape(String[] param) {
        if (param[1].equals("circle")) {
            switch (param.length) {
                case 4:
                    scene.add(new Circle(parsePoint(param[2]), Double.parseDouble(param[3])));
                    System.out.println(new Circle(parsePoint(param[2]), Double.parseDouble(param[3])) + " >>ajouté avec succès<<");
                    break;
                case 5:
                    scene.add(new Circle(parsePoint(param[2]), Double.parseDouble(param[3]), param[4].charAt(0)));
                    System.out.println(new Circle(parsePoint(param[2]), Double.parseDouble(param[3]), param[4].charAt(0)) + " >>ajouté avec succès<<");
                    break;
                default:
                    System.out.println("Erreur , cercle non ajoutée");
            }
        } else if (param[1].equals("rectangle")) {
            switch (param.length) {
                case 5:
                    scene.add(new Rectangle(parsePoint(param[2]), Double.parseDouble(param[3]), Double.parseDouble(param[4])));
                    System.out.println(new Rectangle(parsePoint(param[2]), Double.parseDouble(param[3]), Double.parseDouble(param[4])) + " >>ajouté avec succès<<");
                    break;
                case 6:
                    scene.add(new Rectangle(parsePoint(param[2]), Double.parseDouble(param[3]), Double.parseDouble(param[4]), param[5].charAt(0)));
                    System.out.println(new Rectangle(parsePoint(param[2]), Double.parseDouble(param[3]), Double.parseDouble(param[4]), param[5].charAt(0)) + " >>ajouté avec succès<<");
                    break;
                default:
                    System.out.println("Erreur , rectangle non ajoutée");
            }
        } else if (param[1].equals("square")) {
            switch (param.length) {
                case 4:
                    scene.add(new Square(parsePoint(param[2]), Double.parseDouble(param[3])));
                    System.out.println(new Square(parsePoint(param[2]), Double.parseDouble(param[3])) + " >>ajouté avec succès<<");
                    break;
                case 5:
                    scene.add(new Square(parsePoint(param[2]), Double.parseDouble(param[3]), param[4].charAt(0)));
                    System.out.println(new Square(parsePoint(param[2]), Double.parseDouble(param[3]), param[4].charAt(0)) + " >>ajouté avec succès<<");
                    break;
                default:
                    System.out.println("Erreur , carré non ajoutée");
            }
        }
    }

    public static void parseCmd(String cmd) {
        String[] param = cmd.split(" ");

        switch (param[0]) {
            case "help":
                printHelp();
                break;
            case "newScene":
                scene = new Drawing(parsePoint(param[1]));
                System.out.println("Nouvelle scene de dimension " + parsePoint(param[1]) + " créée avec succès");
                break;
            case "drawScene":
                scene.printShapeList();
                scene.drawIvuritingu();
                scene.printShapeList();
                break;
            case "add":
                parseShape(param);
                break;
            case "move": {
                int id = parseInt(param[1]);
                Point pt = parsePoint(param[2]);
                System.out.println(scene.getShape(id).move(pt.getX(), pt.getY()) + " >>déplacé avec succès<<");
                break;
            }
            case "del":
                System.out.println(scene.delete(parseInt(param[1])) + " >>supprimé avec succès<<");
                break;
            case "setC": {
                int id = parseInt(param[1]);
                char color = param[2].charAt(0);
                System.out.println(scene.getShape(id).setColor(color) + " >>couleur modifiée avec succès<<");
                break;
            }
            case "save":
                saveToFile(param[1]);
                break;
            case "load":
                loadFromFile(param[1]);
                break;
            case "exit":
                System.exit(1);
        }
    }

    public static void printHelp() {
        System.out.println("Les commandes doivent respecter une syntaxe bien préxise , veuillez faire attention à ne pas rajouter d'espace là ou cela n'est pas nécéssaire , les conséquences pourraient etre terribles ...:");
        System.out.println("*Pour creer une nouvelle scene , tapez newScene x,y   où x et y doivent etre des entiers représentant la taille dela nouvelle scene");
        System.out.println("*Pour dessiner la scene et afficher la liste de formes, tapez drawScene");
        System.out.println("*Pour ajouter une forme , tapez add (circle|rectangle|square) x,y (side|width|radius) (height) (color)");
        System.out.println("***Notez que les éléments entre parenthèses signifient qu'il faut faire un choix ou que le parametre est optionel , par exemple : height ne doit pas etre donné si l'on veut creer un cercle");
        System.out.println("***Exemple : add circle 5,3 4 C > créera un cercle de centre (5,3) et de rayon 4 , de couleur C");
        System.out.println("***Exemple2: add square 2,2 4  > créera un carré dont le point haut gauche se situe en (2,2) et ayant des cotés de 4");
        System.out.println("***Exemple3: add rectangle 1,1 5 2 > créera un rectangle dont le point HG se situe en (1,1) en ayant 5 de largeur et 2 de hauteur");
        System.out.println("*Pour bouger une forme, tapez move id x,y");
        System.out.println("*Pour supprimer une forme , tapez del id");
        System.out.println("*Pour changer la couleur d'une forme , tapez setC id letter");
        System.out.println("*Pour afficher tout , tapez drawScene");
        System.out.println("*Pour sauvegarder dans un fichier,tapez save file où file est le nom du fichier");
        System.out.println("*Pour charger depuis un fichier,tapez load file");
        System.out.println("Pour fermer le programme , tapez exit");
    }
}
