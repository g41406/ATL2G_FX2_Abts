/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esi.atl.g41406.paint.view;

import esi.atl.g41406.paint.model.*;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.MenuBar;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
//import javafx.scene.shape.*;

/**
 *
 * @author T5UN4M1
 */
public class ATL2G_TD05_Abts extends Application {
    static int mode;
    public void draw(Drawing drawing,Pane pane){
                
        for(int i=0;i<drawing.getShapes().size();++i){

            if(drawing.getShape(i) instanceof Circle){
                Circle c = (Circle) drawing.getShape(i);
                javafx.scene.shape.Circle circle = new javafx.scene.shape.Circle();
                circle.setCenterX(c.getCenter().getX());
                circle.setCenterY(c.getCenter().getY());
                circle.setRadius(c.getRadius());
                
                pane.getChildren().add(circle);
                
            } else if(drawing.getShape(i) instanceof Rectangle|| drawing.getShape(i) instanceof Square){
                Rectangle r = (Rectangle) drawing.getShape(i);
                javafx.scene.shape.Rectangle rectangle = new javafx.scene.shape.Rectangle();
                rectangle.setX(r.getOrigin().getX());
                rectangle.setY(r.getOrigin().getY());
                rectangle.setWidth(r.getWidth());
                rectangle.setHeight(r.getHeight());
                
                pane.getChildren().add(rectangle);
            }
        }
    }
    @Override
    public void start(Stage primaryStage) {
        
        mode = 0;
        /*
        ¨   mode 0 > nothing
            mode 1 > add circle
            mode 2 > add rectagle
            mode 3 > move
            mode 4 > remove
        */
        
        BorderPane border = new BorderPane();
        
        MenuBar menuBar = new MenuBar();
        
        border.setTop(menuBar);
        
        Menu menuFile = new Menu("File");
        Menu menuHelp = new Menu("?");
        
        
        MenuItem exit = new MenuItem("Exit");
        exit.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                System.exit(0);
            }
        });
        MenuItem help = new MenuItem("HELP");
        help.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
            }
        });
        menuHelp.getItems().add(help);
        menuFile.getItems().add(exit);
        
        menuBar.getMenus().addAll(menuFile,menuHelp);
        
        
        ToolBar toolBar = new ToolBar();
        
        Button modeCircle = new Button("Circle");
        modeCircle.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                mode = 1;
            }
        });
        Button modeRectangle = new Button("Rectangle");
        modeRectangle.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                mode = 2;
            }
        });
        Button modeMove = new Button("Move");
        modeMove.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                mode = 3;
            }
        });
        Button modeRemove = new Button("Remove");
        modeRemove.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                mode = 4;
            }
        });
        toolBar.getItems().addAll(modeCircle,modeRectangle,modeMove,modeRemove);
        
        
        
        
        border.setBottom(toolBar);
        Pane pane = new Pane();
        
        Drawing drawing = new Drawing(300,300);
           
        StackPane root = new StackPane();

        draw(drawing,pane);
        
        
        /*Button btn = new Button();
        btn.setText("Say 'Hello World'");
        
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Hello World!");
            }
        });
        
        root.getChildren().add(btn);*/
        //root.getChildren().add(toolBar);
        
                javafx.scene.shape.Circle circle = new javafx.scene.shape.Circle();
                circle.setCenterX(50);
                circle.setCenterY(50);
                circle.setRadius(50);
                
                pane.getChildren().add(circle);
                
                pane.setPrefHeight(50);
        
                pane.setLayoutY(50);
        border.setCenter(pane);
        pane.setStyle("-fx-background-color: yellow;");
        
        
        root.getChildren().add(pane);
        root.getChildren().add(border);
        Scene scene = new Scene(root, 300, 350);
        
        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
